//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    motto: 'xxoo',
  },

  //读取数据
  bindReadTap: function () {
    var page = this;
    wx.request({
      //url: 'http://api.yii-application.com/apks/32',
      url: 'http://api.yii-application.com/apks',
      header: {
        'Content-Type': 'application/json'
      },
      data: {
        'page': 3
      },
      method: 'GET',
      success: function (res) {
        page.setData({ motto: res.data.content })
        console.log(res.data)
      }
    })
  },

//新增数据
  bindCreateTap: function () {
    var page = this;
    wx.request({
      url: 'http://api.yii-application.com/apks',
      header: {
        'Content-Type': 'application/json'
      },
      method: 'POST',
      data: {
        title: "testing apk",
        content: "content is here",
        author_id: 1,
        status: 2
      },
      success: function (res) {
        page.setData({ motto: res.data.content })
        console.log(res.data)
      }
    })
  },

  //修改数据
  bindUpdateTap: function () {
    var page = this;
    wx.request({
      url: 'http://api.yii-application.com/apks/44',
      header: {
        'Content-Type': 'application/json'
      },
      method: 'PUT',
      data: {
        title: "modified title",
        content: "content is here",
        author_id: 1,
        status: 2
      },
      success: function (res) {
        //page.setData({ motto: res.data.content })
        console.log(res.data)
      }
    })
  },


  //删除数据
  bindDeleteTap: function () {
    var page = this;
    wx.request({
      url: 'http://api.yii-application.com/apks/44',
      header: {
        'Content-Type': 'application/json'
      },
      method: 'DELETE',
      data: {
      },
      success: function (res) {
        //page.setData({ motto: res.data.content })
        console.log(res.data)
      }
    })
  },

  //其他http动词
  bindOtherTap: function () {
    var page = this;
    wx.request({
      url: 'http://api.yii-application.com/apks/32',
      header: {
        'Content-Type': 'application/json'
      },
      method: 'OPTIONS',
      data: {
      },
      success: function (res) {
        //page.setData({ motto: res.data.content })
        console.log(res.data)
      }
    })
  },

  //文章搜索
  bindSearchTap: function () {
    var page = this;
    wx.request({
      url: 'http://api.yii-application.com/apks/search',
      header: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: {
        keyword: 'apk2'
      },
      success: function (res) {
        //page.setData({ motto: res.data.content })
        console.log(res.data)
      }
    })
  },

  //Top 10
  bindTop10Tap: function () {
    var page = this;
    wx.request({
      // url: 'http://api.yii-application.com/top10s',
      url: 'http://api.yii-application.com/top10',
      header: {
        'Content-Type': 'application/json'
      },
      method: 'GET',
      success: function (res) {
        //page.setData({ motto: res.data.content })
        console.log(res.data)
      }
    })
  },

  //登录
  bindLoginTap: function () {
    var page = this;
    wx.request({
      url: 'http://api.yii-application.com/adminuser/login',
      header: {
        // 'Content-Type': 'application/x-www-form-urlencoded'
        //对应服务端用 $model->load(\Yii::$app->getRequest()->getBodyParams(), '');接收数据，不能用_POST[]这种接收
        'Content-Type': 'application/json'
      },
      method: 'POST',
      data: {
        // username: "anal",
        username: "jiangchao",
        password: "123456"
      },
      success: function (res) {
        //page.setData({ motto: res.data.content })
        console.log(res.data)
      }
    })
  },
  //有访问令牌时，读取数据
  bindAuthReadTap: function () {
    var page = this;
    wx.request({
      url: 'http://api.yii-application.com/apks/32?access-token=cSzl_N5KXnCbKntouetxj5RmpGHsvbxK',
      header: {
        'Content-Type': 'application/json'
      },
      method: 'PUT',//GET
      data: {
        page: 1
      },
      success: function (res) {
        page.setData({ motto: res.data.content })
        console.log(res.data)
      }
    })
  },
  //BasicAuth读取数据
  bindBasicAuthReadTap: function () {
    var page = this;
    wx.request({
      url: 'http://api.yii-application.com/apks/32',
      header: {
        'Content-Type': 'application/json'
      },
      method: 'GET',
      data: {
        page: 1,
      },
      success: function (res) {
        page.setData({ motto: res.data.content })
        console.log(res.data)
      }
    })
  },

  //权限检查演示
  bindPrivilegeReadTap: function () {
    var page = this;
    wx.request({
      url: 'http://api.yii-application.com/apks?access-token=cSzl_N5KXnCbKntouetxj5RmpGHsvbxK',
      header: {
        'Content-Type': 'application/json'
      },
      method: 'GET',
      data: {
        page: 1,
      },
      success: function (res) {
        page.setData({ motto: res.data.content })
        console.log(res.data)
      }
    })
  },


  //注册用户
  bindSingupReadTap: function () {
    var page = this;
    wx.request({
      url: 'http://api.yii-application.com/adminuser/signup',
      header: {
        'Content-Type': 'application/json'
      },
      method: 'POST',
      data: {
        username: 'pangpang',
        nickname: '胖胖',
        password: '123456',
        password_repeat: '123456',
        email: 'pangpang@qq.com'
      },
      success: function (res) {
        page.setData({ motto: res.data.content })
        console.log(res.data)
      }
    })
  },


  //速率限制的演示
  bindRateReadTap: function () {
    var page = this;
    wx.request({
      url: 'http://api.yii-application.com/apks/32?access-token=vTSna3ZCVwKpDldP7IGR0EbxO6Oo-Ofv',
      header: {
        'Content-Type': 'application/json'
      },
      method: 'GET',
      data: {
        page: 1,
      },
      success: function (res) {
        page.setData({ motto: res.data.content })
        console.log(res.data)
      }
    })
  },
  //版本化的演示
  bindV2ReadTap: function () {
    var page = this;
    wx.request({
      url: 'http://api.yii-application.com/v2/apks',
      header: {
        'Content-Type': 'application/json'
      },
      method: 'GET',
      data: {
        page: 1,
      },
      success: function (res) {
        page.setData({ motto: res.data.content })
        console.log(res.data)
      }
    })
  },

  //格式化响应
  bindNegoReadTap: function () {
    var page = this;
    wx.request({
      url: 'http://api.yii-application.com/v2/apks',
      header: {
        'Content-Type': 'application/json'
      },
      method: 'GET',
      data: {
        page: 1,
      },
      success: function (res) {
        page.setData({ motto: res.data.content })
        console.log(res.data)
      }
    })
  },

  //自定义错误响应
  bindErrReadTap: function () {
    var page = this;
    wx.request({
      url: 'http://api.yii-application.com/v2/apks',
      header: {
        'Content-Type': 'application/json'
      },
      method: 'PUT',
      data: {
        page: 1,
      },
      success: function (res) {
        page.setData({ motto: res.data.content })
        console.log(res.data)
      }
    })
  },



  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  getUserInfo: function (e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})
